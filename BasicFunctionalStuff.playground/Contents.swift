let words = ["Dreamers", "Tramps", "Coffee Lovers", "Dream Team", "Gazelles", "Unicorns"]

// map the strings to their character counts

let counts =
    words.map { word in word.count}

// use shorthand argument names

let counts2 =
    words.map { $0.count }

// sum the character counts

let sum =
counts.reduce(0, { operand1, operand2 in operand1 + operand2 })

// sum with shorthand argument names

let sum2 =
counts.reduce(0, { $0 + $1 })

// closure after parenthesis

let sum3 =
counts.reduce(0) { $0 + $1 }

// sum even simpler

let sum4 =
counts.reduce(0,+)

// or product

let product =
counts.reduce(1,*)


let numberStrings = ["2", "Ipsum", "4", "simply", "6", "text", "12", "9"]

// convert strings to numbers or nil when the conversion is not possible

let optionalNumbers =
    numberStrings.map { Int($0) }

// filter for the non nil values
let filteredNumbers =
    numberStrings.map { Int($0) }.filter{$0 != nil}

// better solution to filter for the non nil values

let numbers =
    numberStrings.compactMap { Int($0) }

// filter numbers which are less than 10
let smallNumbers
    = numbers.filter{ $0 < 10 }

// average

let smallNumbersAverage =
    smallNumbers.reduce(0,+) / smallNumbers.count

// how to combine arrays, keep the hierachy

let wordsAndNumbers0 =
    [words, numberStrings].map { $0 }

// how to combine arrays, flatten the hierachy

let wordsAndNumbers =
    [words, numberStrings].flatMap { $0 }


let combinedCount =
    [words, numberStrings].flatMap { $0 }.map { $0.count }

